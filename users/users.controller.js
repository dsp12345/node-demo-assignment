﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticateSchema, authenticate);
router.post('/register', authorize(),registerSchema, register);
router.post('/register_admin',registerSchema, register);
router.get('/', authorize(), getAll);
// router.get('/current', authorize(), getCurrent);
// router.get('/:id', authorize(), getById);
 router.put('/:id', authorize(), updateSchema, update);
 router.delete('/:id', authorize(), deleteUser);

module.exports = router;

function authenticateSchema(req, res, next) {
    const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => res.json(user))
        .catch(next);
}

function registerSchema(req, res, next) {
    const schema = Joi.object({
        firstName: Joi.string().regex(/^[a-zA-Z]*$/).max(20).required(),
        middleName: Joi.string().regex(/^[a-zA-Z]*$/).max(20),
        lastName: Joi.string().regex(/^[a-zA-Z]*$/).max(20),
        mobileNo: Joi.string().regex(/^[0-9]*$/).max(10).min(10),
        emailId: Joi.string().email().max(30).required(),
        username: Joi.string().regex(/^[a-zA-Z0-9]*$/).required(),
        password: Joi.string().min(6).required(),
        role: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({ message: 'Registration successful' }))
        .catch(next);
}


function register_admin(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({ message: 'Registration successful' }))
        .catch(next);
}

function getAll(req, res, next) {

    console.log("dsdsd");
    //userService.findAll({where: {role:'USER' }})
    db.User.findAll({ where: {role:'USER' } })
        .then(users => res.json(users))
        .catch(next);


}

function getCurrent(req, res, next) {
    res.json(req.user);
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => res.json(user))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        firstName: Joi.string().regex(/^[a-zA-Z]*$/).max(20).required(),
        middleName: Joi.string().regex(/^[a-zA-Z]*$/).max(20),
        lastName: Joi.string().regex(/^[a-zA-Z]*$/).max(20),
        mobileNo: Joi.string().regex(/^[0-9]*$/).max(10).min(10),
        emailId: Joi.string().email().max(30).required(),
        username: Joi.string().regex(/^[a-zA-Z0-9]*$/).required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(user => res.json(user))
        .catch(next);
}

function deleteUser(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({ message: 'User deleted successfully' }))
        .catch(next);
}

